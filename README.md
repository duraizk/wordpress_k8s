**Deploy wordpress on EC2 instance minikube kubernetes cluster**


Launch a t2.small ec2 instance with public ip on aws

ssh into the instance



First we have to install kubectl 

*  curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl 


*  chmod +x ./kubectl


*  sudo mv ./kubectl /usr/local/bin/kubectl


If Docker is not installed, install it


*  sudo apt-get update && \
    sudo apt-get install docker.io -y

Install Minikube. Minikube is a tool that makes it easy to run Kubernetes locally.

*  curl -Lo minikube https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64 && chmod +x minikube && sudo mv minikube /usr/local/bin/


Start  Minikube

*  minikube start --vm-driver=none

Check the status if minikube is running


*  minikube status

You can install helm through script that will automatically grab the latest version of the Helm client and install it locally.


* curl -LO https://git.io/get_helm.sh
* chmod 700 get_helm.sh
* .
/get_helm.sh

sometimes it compianed socat is missing. Socat is a command line based utility that establishes two bidirectional byte streams and transfers data between them

*  apt-get install -y socat


You have to install Tiller to your running Kubernetes cluster
*  helm init 



Now you have to clone wordpress repository git clone https://gitlab.com/duraizk/wordpress_k8s.git
whcih contains mysql and wordpress helm charts

You can deploy the wordpress and mysql helm charts through
* cd wordpress_k8s
* helm install ./wordpress/
* helm install ./mysql/


you can see the deployments 

*  kubectl get deployments


you can access the wordpress through

http://<public.ip.address.instance>